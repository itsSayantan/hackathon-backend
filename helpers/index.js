const multer = require('multer')
const winston = require('winston')
const xlsx = require('xlsx')

const path = require('path')
const fs = require('fs')
const uuid = require('uuid')

const {
    getRelevantDataFromUploadedFile,
} = require('./experiments')

const {
    TEMPLATE_UPLOAD_FOLDER,
} = require('../constants')

const logger = winston.createLogger({
    format: winston.format.simple(),
    transports: [
        new winston.transports.File({ filename: './logs/logs.log' })
    ]
})

function sendResponse(responseObj, req, res) {
    const date = (new Date).toISOString()
    const ip = req.ip
    const path = req.path
    const method = req.method
    const statusCode = responseObj.status

    // message will always be there for an error case and may or may not be there for success case
    const message = (typeof responseObj.body.message === 'undefined') ? `Successfully executed the API` : responseObj.body.message

    // considering that all error codes are in the range of 400 to 600
    if (statusCode >= 400 && statusCode <= 599) {
        logger.error(`${date}|${ip}|${path}|${method}|${statusCode}|${message}`)
    } else {
        logger.info(`${date}|${ip}|${path}|${method}|${statusCode}|${message}`)
    }

    res.status(responseObj.status).json(responseObj.body)
}

function uploadFile(req, res, next) {
    const uid = uuid()
    req.uid = uid
    console.log(path.join(__dirname + TEMPLATE_UPLOAD_FOLDER + uid))
    fs.mkdir(path.join(__dirname + TEMPLATE_UPLOAD_FOLDER + uid), { recursive: true }, (err) => {
        if (err) throw err;
        // Prints: /tmp/foo-itXde2 or C:\Users\...\AppData\Local\Temp\foo-itXde2

        const storage = multer.diskStorage({
            destination: function (req, file, cb) {
                cb(null, path.join(__dirname + TEMPLATE_UPLOAD_FOLDER + uid))
            },
            filename: function (req, file, cb) {
                if (typeof file.mimetype !== 'undefined' && (file.mimetype === 'application/vnd.ms-excel' || file.mimetype === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')) {
                    cb(null, 'input' + file.originalname.substr(file.originalname.lastIndexOf('.'), file.originalname.length - file.originalname.lastIndexOf('.')))
                }
            }
        })
        const upload = multer({
            fileFilter: (req, file, cb) => {
                // if (typeof file.mimetype !== 'undefined' && (file.mimetype === 'application/vnd.ms-excel' || file.mimetype === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || file.mimetype === 'application/vnd.ms-excel.sheet.macroenabled.12')) {
                cb(null, true)
                // } else {
                //     sendResponse({
                //         status: 400,
                //         body: {
                //             status: false,
                //             message: `File with wrong file format uploaded. Only 'xls' and 'xlsx' file formats are supported.`
                //         }
                //     }, req, res)
                // }
            },
            storage
        })
        upload.single('file')(req, res, next)
    });
}

function readFromExcelDocument(req, res, next) {
    // currently file has to be uploaded
    if (typeof req.file === 'undefined' || typeof req.file.filename === 'undefined') {
        sendResponse({
            status: 400,
            body: {
                status: false,
                message: "File is not provided"
            }
        }, req, res)
    } else {
        const fullFileName = path.join(__dirname + TEMPLATE_UPLOAD_FOLDER + req.uid + '/' + req.file.filename)
        let fileContents
        fileContents = xlsx.readFile(fullFileName)

        req.__templateFileData = fileContents

        next()
    }
}

function deleteTemplateFile(req, res, next) {
    // currently file has to be uploaded
    if (typeof req.file === 'undefined' || typeof req.file.filename === 'undefined') {
        sendResponse({
            status: 400,
            body: {
                status: false,
                message: "File is not provided"
            }
        }, req, res)
    } else {
        fs.unlinkSync(TEMPLATE_UPLOAD_FOLDER + req.file.filename)

        next()
    }
}

module.exports = {
    sendResponse,
    uploadFile,
    readFromExcelDocument,
    deleteTemplateFile,

    getRelevantDataFromUploadedFile,
}