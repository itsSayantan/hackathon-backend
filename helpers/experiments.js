const uuid = require('uuid/v1')

function getAllRowStarterKeysFromUploadedFile(keys, startingColumnAlphabet, STARTING_ROW_NUMBER) {
    const keysWhichStartsWithAAndISFollowedByANumber = keys.filter(k => k.startsWith(startingColumnAlphabet) && k.charAt(1).toString().match(/^[0-9]$/))

    return keysWhichStartsWithAAndISFollowedByANumber.filter(k => Number(k.substr(1, (k.length - 1))) >= STARTING_ROW_NUMBER)
}

function createCamelCasedStringFromWords(str) {
    let restOfTheString = ''
    str.split(' ').map((s, i) => {
        if (i > 0) {
            restOfTheString += s.toLowerCase().charAt(0).toUpperCase() + s.substr(1, s.length - 1)
        }
    })
    return (str.split(' ')[0].toLowerCase() + restOfTheString)
}

async function getRelevantDataFromUploadedFile(data) {
    try {
        console.log(data)
        const starterRows = getAllRowStarterKeysFromUploadedFile(Object.keys(data), 'A', '2')

        if (!starterRows && starterRows.length === 0) {
            return 'NO_ROWS'
        } else {
            const finalObject = {
                columnDefs: [],
                rowData: []
            }
            const headerIdentifier = /^([A-Z]+)(\d+)$/
            const allKeys = Object.keys(data).filter(e => e.match(headerIdentifier))
            
            for (let i = 0; i < allKeys.length; i += 1) {
                const key = allKeys[i]
                const match = key.match(headerIdentifier)
                const rowNumber = parseInt(match[2])
                const columnName = match[1]
            
                if (rowNumber === 1) {
                    finalObject.columnDefs.push({
                        headerName: data[key].w,
                        field: columnName
                    })
                }
                else {
                    const rowData = finalObject.rowData[rowNumber - 2]
            
                    if (rowData)
                        finalObject.rowData[rowNumber - 2] = { ...rowData, [columnName]: data[key].w }
                    else
                        finalObject.rowData[rowNumber - 2] = { [columnName]: data[key].w }
                }
            }

            return finalObject
        }
    } catch (error) {
        console.log(error)
        return 'ERR:'+JSON.stringify(error)
    }

}

module.exports = {
    getRelevantDataFromUploadedFile,
}