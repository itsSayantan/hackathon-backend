const router = require('express').Router()

const {
    uploadFile,
    readFromExcelDocument,
    deleteTemplateFile,
} = require('../helpers')

const services = require('../services')

router.post('/api/createExperiment', uploadFile, readFromExcelDocument, services.createExperiment)


module.exports = router