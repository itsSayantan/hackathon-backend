const dotenv = require('dotenv')
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const app = express()
dotenv.config()

const fallback = require('express-history-api-fallback')
const root = __dirname + '/public'
app.use(express.static(root))
app.use(fallback('index.html', { root: root }))

app.use(cors())
const routes = require('./routes')

app.use(bodyParser.json())

app.use(routes)

app.listen(process.env.PORT, () => console.log(`Server is running on PORT: ${process.env.PORT}`))