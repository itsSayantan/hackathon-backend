const path =  require('path')
const fs = require('fs')
const cp = require('child_process')

const {
    getRelevantDataFromUploadedFile,
    sendResponse,
} = require('../helpers')

const {
    TEMPLATE_UPLOAD_FOLDER,
} = require('../constants')

async function createExperiment(req, res, next) {
    const dataFromExcel = req.__templateFileData

    const data = await getRelevantDataFromUploadedFile(dataFromExcel.Sheets[Object.keys(dataFromExcel.Sheets)[0]])

    const p = path.join(__dirname+TEMPLATE_UPLOAD_FOLDER+req.uid)
    const command = `Rscript /home/vignesh_k/Codes/EDA.R ${p}`;
    console.log(command);

    cp.exec(command, {
        maxBuffer: 5000 * 1024 * 1024
    }, function (err, result, stderr) {
        if (err) {
            sendResponse({
                status: 500,
                body: {
                    status: false,
                    error: err,
                    message: "Something went wrong while running the algorithm during experiment creation."
                }
            }, req, res);
        } else {
            let obj = {}
            fs.readFile(`${p}/summary.json`, 'utf-8', (error, data1) => {
                if (error) {
                    console.log(error)
                    sendResponse({
                        status: 500,
                        body: {
                            status: false,
                            error: error,
                            message: "Something went wrong while running the algorithm during experiment creation."
                        }
                    }, req, res);
                } else {
                    obj['summary'] = JSON.parse(data1)
                    fs.readFile(`${p}/pk.json`, 'utf-8', (error, data2) => {
                        if (error) {
                            console.log(error)
                            sendResponse({
                                status: 500,
                                body: {
                                    status: false,
                                    error: error,
                                    message: "Something went wrong while running the algorithm during experiment creation."
                                }
                            }, req, res);
                        } else {
                            obj['pk'] = JSON.parse(data2)
                            fs.readFile(`${p}/hierarchy.json`, 'utf-8', (error, data3) => {
                                if (error) {
                                    console.log(error)
                                    sendResponse({
                                        status: 500,
                                        body: {
                                            status: false,
                                            error: error,
                                            message: "Something went wrong while running the algorithm during experiment creation."
                                        }
                                    }, req, res);
                                } else {
                                    obj['hierarchy'] = JSON.parse(data3)
                                    fs.readFile(`${p}/hierarchy.json`, 'utf-8', (error, data3) => {
                                        sendResponse({
                                            status: 200,
                                            body: {
                                                status: false,
                                                data: {
                                                    ...data,
                                                    folderName: req.uid,
                                                    ...obj,
                                                }
                                            }
                                        }, req, res)
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}

module.exports = {
    createExperiment,
}